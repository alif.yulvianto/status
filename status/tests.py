from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .models import StatusModel
from .views import status
from .forms import StatusForm
from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.chrome.options import Options

# Create your tests here.
#class StatusFunctionalTest(TestCase):

#    def setUp(self):
#        chrome_options = Options()
#        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#        super(StatusFunctionalTest, self).setUp()

#    def tearDown(self):
#        self.selenium.quit()
#        super(StatusFunctionalTest, self).tearDown()

#    def test_input_todo(self):
#        selenium = self.selenium
#        # Opening the link we want to test
#        selenium.get('http://localhost:8000/status/')
        # find the form element
#        title = selenium.find_element_by_name('your_status')
#        submit = selenium.find_element_by_class_name('button')

        # Fill the form with data
#        title.send_keys('Coba-coba')

        # submitting the form
#        submit.send_keys(Keys.RETURN)

class StatusUnitTest(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_status_template_used(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_object_to_database(self):
        StatusModel.objects.create(content='hai', date=timezone.now())
        count_status = StatusModel.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_form_validated(self):
        form = StatusForm(data={'content':'', 'date':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_status'],
            ['This field is required.']
        )

    def test_status_page_is_completed(self):
        test_str = 'hai'
        response_post = Client().post('/status/', {'content': test_str, 'date': timezone.now()})
        self.assertEqual(response_post.status_code, 200)

