from django.urls import path
from .views import status, delete

urlpatterns = [
    path('', status, name=''),
    path('status/', status, name='status'),
    path('delete-all/', delete, name='delete'),
]
